<div style="margin:0px 50px 0px 50px;">

    @if($pages)

        <table class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th class="text-center">№ п/п</th>
                <th class="text-center">Имя</th>
                <th class="text-center">Псевдоним</th>
                <th class="text-center">Текст</th>
                <th class="text-center">Дата создания</th>
                <th class="text-center">Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $k=>$page)

                <tr>
                    <td>{{$page->id}}</td>
                    <td>{!! Html::link(route('pagesEdit',['page'=>$page->id]),$page->name,['alt'=>$page->name]) !!}</td>
                    <td>{{$page->alias}}</td>
                    <td>{{$page->text}}</td>
                    <td>{{$page->created_at}}</td>
                    <td>
                    {!! Form::open(['url'=>route('pagesEdit',['page'=>$page->id]),'class'=>'form-horizontal','method'=>'POST']) !!}

                        {{method_field('DELETE')}}
                        {!! Form::button('Удалить',['class'=>'btn btn-danger','type'=>'submit']) !!}

                    {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
        {!! Html::link(route('pagesAdd'),'Нова сторінка',['alt'=>$page->name]) !!}
    @endif



</div>