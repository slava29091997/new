<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use App\Service;
use App\Portfolio;
use App\People;
use PhpParser\Node\Expr\Array_;
use DB;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    //
    public function execute(Request $request){

        if ($request->isMethod('post')){   // Провіряємо якого типу запрос

            $messages =[    //Формуємо свої ошибки повідомлень
                'required'=>'Поле :attribute обовязкове для заповнення',
                'email'=>'Поле :attribute повине бути email адресом'
            ];

            $this->validate($request,[   // Валідіруємо текст

                'name'=>'required|max:255',
                'email'=>'required|email',
                'text'=>'required'

            ],$messages
            );

            $data = $request->all(); // Сохраняємо в зміну передані дані з форми в вигляді масиву

            $result = Mail::send('site.email',['data'=>$data],function ($message) use ($data) {//Отправка email сообщеній
                $mail_admin = env('MAIL_ADMIN'); //функція що бере дані з ENV

                $message->from($data['email'],$data['name']);//функця яка встановлює відкого відпривляється пісьмо
                $message->to($mail_admin)->subject('Question');//функція показує куда отправити пісьмо   subject - Тема пісьма

            });

            if ($result){
                return redirect()->route('home')->with('status','Email is send');
            }

        }



        $pages = Page::all();    //Получаємо дані з бази даних
        $portfolios = Portfolio::get(array('name','filter','images'));
        $services = Service::where('id','<',20)->get();
        $people = People::take(3)->get();

        $tags = DB::table('partfolios')->distinct()->pluck('filter'); //Вибираємо неповторяющіся записі з бази даних



        $menu = array();
        foreach ($pages as $page){
            $item = array('title'=>$page->name,'alias'=>$page->alias);
            array_push($menu,$item);
        }

        $item = array('title'=>'Services','alias'=>'service');
        array_push($menu,$item); //Приєднуємо дані в масив

        $item = array('title'=>'Portfolio','alias'=>'Portfolio');
        array_push($menu,$item);

        $item = array('title'=>'Team','alias'=>'team');
        array_push($menu,$item);

        $item = array('title'=>'Contact','alias'=>'contact');
        array_push($menu,$item);



        return view('site.index',array(  //Передаємо дані в Види
            'menu'=>$menu,
            'pages'=>$pages,
            'services'=>$services,
            'portfolios'=>$portfolios,
            'people'=>$people,
            'tags'=>$tags
        ));
    }

}
