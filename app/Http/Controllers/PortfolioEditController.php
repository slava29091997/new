<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use Validator;

class PortfolioEditController extends Controller
{
    public function execute(Portfolio $portfolio,Request $request)
    {

        if ($request->isMethod('delete')){
            $portfolio ->delete();
            return redirect('admin')->with('status','Сторінка удаліна');
        }




        if ($request->isMethod('post')){

            $input = $request->except('_token');

            $massages =[
                'required'=>'Поле :attribute обовязкове для заповнення'
            ];

            $validator = Validator::make($input,[
                'name'=>'required|max:255',
                'filter'=>'required|max:255',
            ],$massages);


            if ($validator->fails()){
                return redirect()
                    ->route('portfolioEdit',['page'=>$input['id']])
                    ->withErrors($validator);
            }
            if ($request->hasFile('images')){
                $file = $request->file('images');

                $file->move(public_path().'/assets/img',$file->getClientOriginalName());

                $input['images']= $file->getClientOriginalName();

            }
            else{
                $input['images']= $input['old_images'];
            }
            unset($input['old_images'])
                ;
            $portfolio->fill($input);

            if ($portfolio->update()){
                return redirect('admin')->with('status','Сторінка змінена');
            }
        }





        $old = $portfolio->toArray();

        if (view()->exists('admin.pages_edit')){
            $data = [
                'title'=>' Редактірування портфоліо - '.$old['name'],
                'data'=>$old
            ];
            return view('admin.portfolio_edit',$data);
        }
    }
}
