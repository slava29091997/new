<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PortfolioAddController extends Controller
{
    public function execute(Request $request){

        if($request->isMethod('post')){
            $input = $request->except('_token');


            $massages =[
                'required'=>'Поле :attribute обовязкове для заповнення',
            ];


            $validator = Validator::make($input,
                [
                    'name'=>'required|max:255',
                    'filter'=>'required|max:255',
                ],$massages);

            if ($validator->fails()){
                return redirect()->route('portfolioAdd')->withErrors($validator)->withInput();
            }

            if ($request->hasFile('images')){
                $file = $request->file('images');
                $input['images'] = $file->getClientOriginalName();

                $file->move(public_path().'/assets/img',$input['images']);
                //return redirect()->route('pagesAdd');
            }

            $portfolio = new Portfolio(); //добавленія до БД

            $portfolio->fill($input); //автозаповнення БД

            if ($portfolio->save()){
                return redirect('admin')->with('status','Сторінка добавлена');
            }

        }




        if (view()->exists('admin.portfolios_add')){

            $data = [
                'title' =>'Нова сторінка'
            ];

            return view('admin.portfolios_add',$data);
        }

        abort(404);
    }
}
