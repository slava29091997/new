<?php

namespace App\Http\Controllers;


use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PagesAddController extends Controller
{
    public function execute(Request $request){


        if($request->isMethod('post')){
           $input = $request->except('_token');


           $massages =[
               'required'=>'Поле :attribute обовязкове для заповнення',
               'unique'=>'Поле :attribute уже існує'
           ];


           $validator = Validator::make($input,
               [
                   'name'=>'required|max:255',
                   'alias'=>'required|unique:pages|max:255',
                   'text'=>'required'
               ],$massages);
           if ($validator->fails()){
               return redirect()->route('pagesAdd')->withErrors($validator)->withInput();
           }

           if ($request->hasFile('images')){
               $file = $request->file('images');
               $input['images'] = $file->getClientOriginalName();

               $file->move(public_path().'/assets/img',$input['images']);
               //return redirect()->route('pagesAdd');
           }

           $page = new Page(); //добавленія до БД

           $page->fill($input); //автозаповнення БД

            if ($page->save()){
                return redirect('admin')->with('status','Сторінка добавлена');
            }

        }


        if (view()->exists('admin.pages_add')){

            $data = [
              'title' =>'Нова сторінка'
            ];

            return view('admin.pages_add',$data);
        }

        abort(404);
    }
}
