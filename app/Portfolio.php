<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'partfolios';

    protected $fillable = ['name','filter','images'];
}
